
function showHideDescription() {
    $(this).find(".description").toggle();
}

function prix(){
	var prix = $("input[name='type']:checked").data("price")*$(".nb-parts input").val()/6;
	prix += $("input[name='pate']:checked").data("price") || 0 ;
	
	$("input[name='extra']:checked").each(function(i, element) {
		prix += $(element).data("price");
	});

	$(".tile p").text(prix + " Euros");
}

$(document).ready(function(){
    $('.pizza-type label').hover(
    showHideDescription,
    showHideDescription
    );

    $('.nb-parts input').on('keyup', function() {
        $(".pizza-pict").remove();
        var pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();

        for(i = 0; i < slices/6 ; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);

		prix();
		
    });

	$('.next-step').click(function() {
		$('.infos-client').show();
		$('.next-step').hide();

        
	});

    $('.add').click(function() {
	    $(".infos-client .add").before("<br><input type='text'/>");
    });

	$('.done').click(function() {
		var nom = $(".infos-client .type:first input").val();
		

		$('.main').empty()
			.append("<p>Merci "+ nom +" ! Votre commande sera livree dans 15 minutes.</p>");

	})

	$("input[data-price]").change(prix);
});
